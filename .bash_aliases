# Bash
alias resrc='source ~/.bashrc'
alias la='ls -A'
alias quit='exit'

alias prettypath="echo $PATH | sed 's/:/\n/g'"

# Git
alias g='git'
alias gcm='git checkout master'
alias gcq='git checkout qa'
alias gcp='git checkout production'
alias gst='git status'
alias gd='git diff'
alias gd1='git diff HEAD~1'
alias gd2='git diff HEAD~2'
alias gdst='git diff --staged'
alias gbr='git branch'
alias gbrm='git branch --merged | egrep -v "(^\*|master|dev|qa|staging|production)"'
alias gbrmd='git branch --merged | egrep -v "(^\*|master|dev|qa|staging|production)" | xargs git branch -d'
alias grpo='git remote prune origin'
alias gaa='git add --a'
alias gap='git add -N . && git add -p'
alias gfmm='git fetch; git merge --ff-only master origin/master'
alias gf='git fetch'
alias glb='git cherry -v master'
alias glau="git ls-files -v | grep '^[[:lower:]]'"
alias gco='git checkout'
alias gdms='git diff --stat master'

alias gdpm='git diff --stat --color production..master'
alias gdpq='git diff --stat --color production..qa'
alias gdqm='git diff --stat --color qa..master'
#__git_complete gco _git_checkout

alias gundo='echo "git reset HEAD~"'

alias gs='echo "You meant gst"'

alias cdgr='cd $(git rev-parse --show-toplevel)'

# Rails
alias gibbi='gem install bundler && bundle install && alert "bundle install complete" || alert "Something went wrong!"'
alias berrl='bundle exec rake routes | less'
alias bex='bundle exec '
alias rc='rails console'
alias rs='rails server'
alias rdesd='rails db:environment:set RAILS_ENV=development'
alias rdtp='rake db:test:prepare'
alias raub='rails app:update:bin'

rdr() {
  rake db:restore FILE=$1
}

# Docker
alias dc='docker compose '

# Android
alias adbks='sudo adb kill-server && sudo adb start-server && adb devices'

# Tmux
alias tmuxa='tmux attach -t'

# Dev Chromium
alias devchromium1="chromium-browser --disable-web-security --user-data-dir=\"/home/alex/.chrome-dev-data/1\" &"
alias devchromium2="chromium-browser --disable-web-security --user-data-dir=\"/home/alex/.chrome-dev-data/2\" &"
alias devchromium="devchromium1"

# ADB
alias adbks="adb kill-server && adb start-server && adb devices"

# xclip
alias xclip="xclip -selection c"

alias gronk="ngrok http -subdomain=amp 3000"

# 10fw
alias dovpn="sudo openvpn --config /opt/10fw-alex.ovpn"
alias mdvpn="sudo openvpn --config /opt/doorman.ovpn"

# v important aliases
alias 🐱="echo MEOW"

qrify() {
  if [ -z "$1" ]
  then
    echo "URL Required"
  else
    qrencode -l H '$1' -o /tmp/qr.png && eog /tmp/qr.png
  fi
  return 0
}

rvmset() {
  if [[ -f ".ruby-version" && -f ".ruby-gemset" ]]; then
    GEMSET="$(cat .ruby-version)@$(cat .ruby-gemset)"
    echo "Running: rvm use $GEMSET --create"
    rvm use $GEMSET --create
  else
    echo "No .ruby-version or .ruby-gemset files found in current directory. Aborting"
  fi
}

capsctrl() {
  pkill xcape
  setxkbmap -option 'caps:ctrl_modifier'
  xcape -e 'Caps_Lock=Escape'
}

turnoffcapslock() {
  python -c 'from ctypes import *; X11 = cdll.LoadLibrary("libX11.so.6"); display = X11.XOpenDisplay(None); X11.XkbLockModifiers(display, c_uint(0x0100), c_uint(2), c_uint(0)); X11.XCloseDisplay(display)'
}
alias TURNOFFCAPSLOCK='turnoffcapslock'
