[[ -s "$HOME/.profile" ]] && source "$HOME/.profile" # Load the default .profile

export ANSIBLE_NOCOWS=1

case $- in *i*) . ~/.bashrc;; esac

[[ -s "$HOME/.rvm/scripts/rvm" ]] && source "$HOME/.rvm/scripts/rvm" # Load RVM into a shell session *as a function*
